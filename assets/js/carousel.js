var elms = document.getElementsByClassName( 'splide' );
console.log(elms);
for ( var i = 0, len = elms.length; i < len; i++ ) {
	new Splide( elms[ i ], {
		type: 'loop',
		autoplay: true,
		pauseOnHover: false,
		pauseOnFocus: false
	} ).mount();
}