---
layout: contact
title: Infos & contact - La MAGMA
show-link: true
menu-name: Infos et contact
order: 2
team:
  members:
    - image: assets/img/portraits/Magma-portrait-1.png
      name: Lucile Martin
      poste: Chargée de coordination
    - image: assets/img/portraits/Magma-portrait-2.png
      name: Réna Nénot
      poste: Présidente
    - image: assets/img/portraits/Magma-portrait-3.png
      name: Rémi Fanget
      poste: Vice-Président
    - image: assets/img/portraits/Magma-portrait-4.png
      name: Pascal Gougeon
      poste: Secrétaire
    - image: assets/img/portraits/Magma-portrait-5.png
      name: Quentin Mascarino
      poste: Trésorier
    
---

